package ua.dp.levelup.dao;
import ua.dp.levelup.core.model.Seat;

import java.util.List;

public interface SeatDao {
    void createSeat(Seat seat);
    List<Seat> getAllSeats();
    Seat getSeatById(Long id);
    void updateSeat(Seat seat);
    void deleteSeat(Seat seat);
}