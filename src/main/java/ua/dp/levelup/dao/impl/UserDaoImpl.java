package ua.dp.levelup.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.levelup.core.model.User;
import ua.dp.levelup.dao.UserDao;

import java.util.*;

/**
 * Created by java on 20.06.2017.
 */
@Repository
@Transactional
public class UserDaoImpl implements UserDao {

    private Map<Long, User> userByIdMap = new HashMap<>();
    private Map<String, Long> idByEmailMap = new HashMap<>();

    @Autowired
    private HibernateTemplate template;

    public void init() {
        List<User> users = Arrays.asList(
                new User("user1@yopmail.com","ff","Alan","Pugachov"),
                new User("user2@yopmail.com","dd", "Bob", "Dilan")
        );

        Random random = new Random();

       /* for (User u : users) {
            u.increaseBalance(random.nextInt(100) + 25);

            createUser(u);
        }*/

        System.out.println("Init say Hello");
    }

    @Override
    public List<User> getAllUsers() {
        return (List<User>) userByIdMap.values();
    }

    @Override
    public User getUserByEmail(String email) {
        Long userId = idByEmailMap.get(email);
        return userByIdMap.get(userId);
    }

    @Override
    public User getUserById(Long id) {
        return userByIdMap.get(id);
    }

    @Override
    public void createUser(User user) {
        template.save(user);
        userByIdMap.put(user.getId(), user);
        idByEmailMap.put(user.getEmail(), user.getId());
    }

    @Override
    public void deleteUser(User user) {
        userByIdMap.remove(user.getId());
        idByEmailMap.remove(user.getEmail());
    }

    @Override
    public void updateUser(User user) {
        userByIdMap.put(user.getId(), user);
        idByEmailMap.put(user.getEmail(), user.getId());
    }
}
