package ua.dp.levelup.core.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ua.dp.levelup.core.model.Film;
import ua.dp.levelup.core.model.MovieSession;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MovieSessionDto {

    private Film film;
    private List<MovieSession> sessionList;
}
