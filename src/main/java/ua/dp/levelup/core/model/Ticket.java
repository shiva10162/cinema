package ua.dp.levelup.core.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;

/**
 * @author Alexandr Shegeda on 23.06.17.
 */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "TICKETS")
public class Ticket {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long ticketId;
  private double price;
  private long movieSessionId;
  @ManyToOne(fetch = FetchType.EAGER)
  @JsonManagedReference
  private Order order;

  private int rowNumber;
  private int seatNumber;
  private Long hallId;


  public Ticket(double price, long movieSessionId, Order order, int rowNumber, int seatNumber) {
    this.price = price;
    this.movieSessionId = movieSessionId;
    this.order = order;
    this.rowNumber = rowNumber;
    this.seatNumber = seatNumber;
  }

  public Ticket(Long ticketId, double price, long movieSessionId, int rowNumber, int seatNumber, Long hallId) {
    this.ticketId = ticketId;
    this.price = price;
    this.movieSessionId = movieSessionId;
    this.rowNumber = rowNumber;
    this.seatNumber = seatNumber;
    this.hallId = hallId;
  }
}
