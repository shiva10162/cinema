package ua.dp.levelup.service;

import ua.dp.levelup.core.model.Order;

import javax.mail.MessagingException;
import java.io.IOException;

public interface EmailManagementService {

    void sendEmail(String recipient, String subject, String emailMessage) throws MessagingException;

    void sendNotificationEmailAfterTicketBuy(String recipient, Order order) throws MessagingException, IOException;

}
