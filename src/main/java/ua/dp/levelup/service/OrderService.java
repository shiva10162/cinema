package ua.dp.levelup.service;

import ua.dp.levelup.core.model.Order;
import ua.dp.levelup.core.model.Ticket;

import java.util.List;

public interface OrderService {

    void createOrder(Ticket... tickets);
    void updateOrder(Order order);
    List<Order> getAllOrders();
    Order getOrderById(long orderId);
    List<Order> getAllOrdersForToday();
    double getDailyProfit();
    Integer getDailyAmount();
}
