package ua.dp.levelup.service.impl;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ua.dp.levelup.core.model.Film;
import ua.dp.levelup.core.model.MovieSession;
import ua.dp.levelup.core.model.filters.Filters;
import ua.dp.levelup.dao.FilmDao;
import ua.dp.levelup.service.FilmService;
import ua.dp.levelup.utils.CinemaUtilityFunctions;

import java.io.*;
import java.util.*;

@Service
public class FilmServiceImpl implements FilmService {

    private FilmDao filmDao;

    @Autowired
    private CinemaUtilityFunctions cinemaUtilityFunctions;

    @Autowired
    private Filters filters;

    @Value("${storage.images.value}")
    private String storage;

    @Autowired
    public void setFilmDao(final FilmDao filmDao) { this.filmDao = filmDao; }

    @Override
    public void createFilm(Film film) {
        filmDao.createFilm(film);
    }

    @Override
    public List<Film> getAllFilms() {
        return filmDao.getAllFilms();
    }

    @Override
    public Film getFilmById(long id) {
        return filmDao.getFilmById(id);
    }

    @Override
    public void updateFilm(Film film) {
        filmDao.updateFilm(film);
    }

    @Override
    public void deleteFilm(Film film) {
        filmDao.deleteFilm(film);
    }

    @Override
    public String upDateNameImage(String nameImage) {
        String timestamp = cinemaUtilityFunctions.formatDateForImage();
        return timestamp + nameImage;
    }

    @Override
    public byte[] getFilmImage(Film film) throws IOException {
        String imageFilmName = film.getImageName();
        File imageFile = new File(storage + File.separator + imageFilmName);
        InputStream in = new FileInputStream(imageFile);
        return IOUtils.toByteArray(in);
    }

    @Override
    public Set<Film> getUniqueFilmsFromSessions(List<MovieSession> sessions) {
        Collections.sort(sessions, filters.sessionComparator());
        Set<Film> uniqueFilms = new LinkedHashSet<>();
        for (MovieSession ms: sessions) {
            uniqueFilms.add(ms.getFilm());
        }
        return uniqueFilms;
    }

    @Override
    public void uploadImage(MultipartFile file, Film film) throws IOException {
        byte[] bytes = file.getBytes();
        File dir = new File(storage);
        if (!dir.exists()) dir.mkdirs();
        String originalImageName = file.getOriginalFilename();
        String fileExtension = originalImageName.substring(originalImageName.lastIndexOf('.'));
        String newImageName = upDateNameImage(film.getName()) + fileExtension;
        film.setImageName(newImageName);
        updateFilm(film);
        File serverFile = new File(dir.getCanonicalPath() + File.separator + newImageName);
        BufferedOutputStream stream = new BufferedOutputStream( new FileOutputStream(serverFile));
        stream.write(bytes);
        stream.close();
    }
}
