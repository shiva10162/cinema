package ua.dp.levelup.service;

import org.springframework.web.multipart.MultipartFile;
import ua.dp.levelup.core.model.Film;
import ua.dp.levelup.core.model.MovieSession;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;

public interface FilmService {

    void createFilm(Film film);
    List<Film> getAllFilms();
    Film getFilmById(long id);
    void updateFilm(Film film);
    void deleteFilm(Film film);
    String upDateNameImage(String nameImage);
    void uploadImage(MultipartFile file, Film film) throws IOException;
    byte[] getFilmImage(Film film) throws IOException;
    Set<Film> getUniqueFilmsFromSessions (List<MovieSession> sessions);
}
