package ua.dp.levelup.utils;

import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class CinemaUtilityFunctions {
    public boolean isSameDay(Date date, Date dateToCompare) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        return fmt.format(date).equals(fmt.format(dateToCompare));
    }

    public List<Date> getFilterDays(){
        long msInDay = 86400000; //количество миллисекунд в 1 дне
        long msInToday = new Date().getTime();
        List<Date> dateList = new ArrayList<>();
        for (int i = 0; i < 5; i++){
            dateList.add(i, new Date(msInToday + (i * msInDay)));
        }
        return dateList;
    }

    public String formatDateForImage(){
        DateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmss");
        return fmt.format(new Date());
    }
}
