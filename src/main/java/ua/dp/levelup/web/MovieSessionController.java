package ua.dp.levelup.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.dp.levelup.core.model.dto.MovieSessionDto;
import ua.dp.levelup.core.model.*;
import ua.dp.levelup.service.HallService;
import ua.dp.levelup.service.MovieSessionService;
import ua.dp.levelup.service.TicketService;

import java.util.*;

@Controller
@RequestMapping("/movie")
public class MovieSessionController {

    @Autowired
    private MovieSessionService movieSessionService;

    @Autowired
    private HallService hallService;

    @Autowired
    private TicketService ticketService;

    @RequestMapping(value = "/add-session", method = RequestMethod.GET)
    public ModelAndView getAddMovieSessionPage() {
        ModelAndView modelAndView = new ModelAndView("add-movie-session");
        modelAndView.addObject("session", new MovieSession());
        return modelAndView;
    }

    @RequestMapping(value = "/add-session", method = RequestMethod.POST, consumes = "application/json")
    public String addMovieSession(@RequestBody MovieSession session) {
        movieSessionService.createMovieSession(session);

        return "redirect:list";
    }

    @RequestMapping("/list")
    public ModelAndView getAllMovieSessions() {
        List<MovieSession> allMovieSessions = movieSessionService.getAllMovieSessions();
        ModelAndView modelAndView = new ModelAndView("movie-session-page");
        modelAndView.addObject("allMovieSessions", allMovieSessions);
        return modelAndView;
    }

    @RequestMapping("/list/{date}")
    public ModelAndView getMovieSessionsByDate(@PathVariable("date") Date dateSessions) {
        List<MovieSession> movieSessionsByDate = movieSessionService.getMovieSessionsByDate(dateSessions);
        ModelAndView modelAndView = new ModelAndView("movie-session-today-page");
        modelAndView.addObject("allMovieSessionsToday", movieSessionsByDate);
        return modelAndView;
    }

    @RequestMapping(value = "/session/{sessionId}")
    public ModelAndView viewMovieSession(@PathVariable Long sessionId) {
        ModelAndView modelAndView = new ModelAndView("view-session");
        List<Ticket> ticketsOfMovieSession = ticketService.getTicketsByMovieSessionId(sessionId);
        MovieSession movieSession = movieSessionService.getMovieSessionById(sessionId);
        int hallNumber = movieSession.getHallNumber();
        Hall hall = hallService.getHallByNumber(hallNumber);

        //////////////////////////////////////////////////////
        List<Row> rows = new ArrayList<>();
        List<Seat> standartSeatsList = new ArrayList<>();
        List<Seat> luxSeatsList = new ArrayList<>();
        for (int i = 0; i < 18; i++){
            standartSeatsList.add(new Seat(i+1, SeatType.COMFORT));
            luxSeatsList.add(new Seat(i+1, SeatType.LUX));
        }
        for (int i = 0; i < 11; i++) {
            rows.add(new Row(i + 1, standartSeatsList));
        }
        rows.add(new Row(12, luxSeatsList));
        hall.setRows(rows);

        //////////////////////////////////////////////////////

        modelAndView.addObject("hall", hall);
        modelAndView.addObject("tickets", ticketsOfMovieSession);

        return modelAndView;
    }

    @RequestMapping("/session/list")
    public ModelAndView getAllMovieSession(@DateTimeFormat(pattern="MM/dd/yyyy")@RequestParam(name = "date", required = false) Date date) {
        if (date == null) {
            date = new Date();
        }

        List<MovieSessionDto> allMovieSessions = movieSessionService.getAllMovieSessionByDate(date);
        ModelAndView modelAndView = new ModelAndView("movie-session");

        modelAndView.addObject("allMovieSessions", allMovieSessions);

        return modelAndView;
    }


}
