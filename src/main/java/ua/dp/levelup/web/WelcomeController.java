package ua.dp.levelup.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ua.dp.levelup.core.model.Film;
import ua.dp.levelup.core.model.MovieSession;
import ua.dp.levelup.service.FilmService;
import ua.dp.levelup.service.MovieSessionService;
import ua.dp.levelup.utils.CinemaUtilityFunctions;

import java.util.*;

@Controller
@RequestMapping("/")
public class WelcomeController {

    @Autowired
    private MovieSessionService movieSessionService;

    @Autowired
    private FilmService filmService;

    @Autowired
    private CinemaUtilityFunctions cinemaUtilityFunctions;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView getAllMovieSessionsToday() {
        List<MovieSession> allMovieSessionsToday = movieSessionService.getAllMovieSessionsForToday();
        ModelAndView modelAndView = new ModelAndView("movie-session-page");
        List<Date> filterDays = cinemaUtilityFunctions.getFilterDays();
        Set<Film> uniqueFilms = filmService.getUniqueFilmsFromSessions(allMovieSessionsToday);

        modelAndView.addObject("filterDays", filterDays);
        modelAndView.addObject("allMovieSessionsToday", allMovieSessionsToday);
        modelAndView.addObject("uniqueFilms", uniqueFilms);
        return modelAndView;
    }
}
