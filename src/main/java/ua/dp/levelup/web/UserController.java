package ua.dp.levelup.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ua.dp.levelup.core.model.User;
import ua.dp.levelup.service.UserService;


@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;


    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView getRegistrationPage() {
        ModelAndView modelAndView = new ModelAndView("registration");
        modelAndView.addObject("session", new User());
        return modelAndView;
    }

    @RequestMapping(value = "/registration",method = RequestMethod.POST, consumes = "application/json")
    public String addUserFromRegistration(@RequestBody User user){
        userService.createUser(user);

        System.out.println(user);

        return "redirect: /film/list";
    }

}
