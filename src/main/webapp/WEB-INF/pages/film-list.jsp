<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Film-list</title>
    <link rel="stylesheet" type="text/css"
          href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cerulean/bootstrap.min.css">
</head>
<body>

<h1>Film list:</h1>
<table class="table table-striped">
    <tr>
        <th>filmId</th>
        <th>name</th>
        <th>description</th>
        <th>duration</th>

    </tr>
    <c:forEach var="film" items="${allFilms}">
    <tr>
        <td>${film.filmId}</td>
        <td>${film.name}</td>
        <td>${film.description}</td>
        <td>${film.duration}</td>
    </tr>
    </c:forEach>
</body>
</html>
