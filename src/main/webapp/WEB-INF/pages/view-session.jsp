<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: unike
  Date: 19.07.2017
  Time: 20:42
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/CSS/view-session.css">
    <title>View Session</title>

</head>
<body>

<div align="center" id="main">
    <img width="1000" src="<c:url value='${pageContext.request.contextPath}/resources/images/screen.png'/>"/>
</div>
<script type="text/javascript">

    var table = document.getElementById("main");
    var rowsNumberInHall = ${hall.rows.size()};


    var row = 1;
    var seat = 1;

    ///
    <c:forEach var="row" items="${hall.rows}">
    table.innerHTML = table.innerHTML + '<div align="center" id="table-seats">';
    <c:forEach var="seat" items="${row.seats}">
    var typeOfSeat = ${seat.seatType.getType()};
    if (typeOfSeat === 1) {
        table.innerHTML = table.innerHTML + '<div id="btn_buy" onclick="reserveSeat()" class="empty_seat"><em>'+
            row + " Ряд, " + seat + " место" +
            '<i></i></em></div>';
    } else if (typeOfSeat === 2) {
        if (seat % 2 === 0) {
            table.innerHTML = table.innerHTML +  '<div id="btn_buy" onclick="reserveSeat()" class="empty_right_lux_seat"><em>'+
                row + " Ряд, " + seat + " место" +
                '<i></i></em></div>';
        } else {
            table.innerHTML = table.innerHTML +  '<div id="btn_buy" onclick="reserveSeat()" class="empty_left_lux_seat"><em>' +
                row + " Ряд, " + seat + " место" +
                '<i></i></em></div>';
        }
    }
    ///////////////

    var btn_buy = document.getElementById("btn_buy");

        btn_buy.id = "but" + row + seat;
    btn_buy.onclick = function (event) {
        reserveSeat();
    };
    seat++;
    </c:forEach>
    table.innerHTML = table.innerHTML + '</div>';
    seat = 1;
    row++;
    </c:forEach>




    //добавть резерв места по клику
    function reserveSeat() {
        console.log(event.currentTarget);
        var btn = event.currentTarget;
        var reserve;
        if (btn.classList.contains("empty_seat")) {
            btn.classList.remove("empty_seat");
            reserve = "empty_seat";
            btn.classList.add("reserved_seat");
        } else if (btn.classList.contains("empty_left_lux_seat")) {
            btn.classList.remove("empty_left_lux_seat");
            reserve = "empty_left_lux_seat";
            btn.classList.add("reserved_left_lux_seat");
        } else if (btn.classList.contains("empty_right_lux_seat")) {
            btn.classList.remove("empty_right_lux_seat");
            reserve = "empty_right_lux_seat";
            btn.classList.add("reserved_right_lux_seat");
        }

        btn.onclick = function (event) {
            unreserveSeat(reserve);
        };

    }

    //снять резерв места по клику
    function unreserveSeat(reserve) {
        var btn = event.currentTarget;
        btn.classList.remove("reserved_seat");
        btn.classList.remove("reserved_left_lux_seat");
        btn.classList.remove("reserved_right_lux_seat");
        btn.classList.add(reserve);
        btn.onclick = function () {
            reserveSeat();
        }
    }


    //отобразить заполненные места
    <c:forEach var="ticket" items="${tickets}">
    var rowNumber = <c:out value = "${ticket.rowNumber}"/>;
    var seatNumber = <c:out value = "${ticket.seatNumber}"/> ;
    console.log(rowNumber);
    console.log(seatNumber);
    var btn = document.getElementById("but"+rowNumber+seatNumber);
    btn.classList.remove("empty_seat");
    btn.classList.remove("empty_left_lux_seat");
    btn.classList.remove("empty_right_lux_seat");
    btn.classList.add("used_seat");
    btn.onclick = "";
    </c:forEach>
</script>

</body>
</html>
