<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Дмитрий
  Date: 25.07.2017
  Time: 22:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>registration</title>
    <link rel="stylesheet" type="text/css"
          href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/cerulean/bootstrap.min.css">
    <style type="text/css">
        html, body {
            height: 0%;
        }

        html {
            display: table;
            margin: 10px;
        }

        body {
            display: table-cell;
            vertical-align: middle;
        }
    </style>
</head>
<body >
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId            : '672878739575397',
            autoLogAppEvents : true,
            xfbml            : true,
            version          : 'v2.10'
        });
        FB.AppEvents.logPageView();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "http://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));


    // This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response) {
        if (response.status === 'connected') {
            // Logged into your app and Facebook.
            _i();
        } else if (response.status === 'not_authorized') {
            // The person is logged into Facebook, but not your app.
            document.getElementById('status').innerHTML = 'Please log ' +
                    'into this app.';
        }
    }

    function _login() {
        FB.login(function(response) {
            // handle the response
            if(response.status==='connected') {
                _i();
            }
        }, {scope: 'public_profile,email'});
    }

    function _i(){
        FB.api('/me?fields=first_name,last_name,email,id', function(response) {
            document.getElementById("firstName").value = response.first_name;
            document.getElementById("lastName").value = response.last_name;
            document.getElementById("email").value = response.email;
        });
    }

</script>
<div id="legend">
    <legend class="">Register</legend>
</div>

<div class="control-group">
    <!-- E-mail -->
    <label class="control-label" for="email">E-mail</label>
    <div class="controls">
        <input type="email" id="email" name="email" placeholder="" class="input-xlarge">
        <p class="help-block">Please provide your E-mail</p>
    </div>
</div>

<%--<span>Email:</span> <br> <input type="text" name="email" id="email"><br>--%>

<div class="control-group">
    <!-- Password-->
    <label class="control-label" for="password">Password</label>
    <div class="controls">
        <input type="password" id="password" name="password" placeholder="" class="input-xlarge">
        <p class="help-block">Password should be at least 4 characters</p>
    </div>
</div>

<%--<span>Password:</span><br><input type="text" name="password" id="password"><br>--%>

    <div class="control-group">
        <!-- Username -->
        <label class="control-label"  for="firstName">First Name</label>
        <div class="controls">
            <input type="text" id="firstName" name="firstName" placeholder="" class="input-xlarge">
            <p class="help-block">Username can contain any letters without spaces</p>
        </div>
    </div>
<%--<span>First Name:</span><br><input type="text" name="firstName" id="firstName"><br>--%>

    <div class="control-group">
        <!-- Password -->
        <label class="control-label"  for="lastName">Password (Confirm)</label>
        <div class="controls">
            <input type="text" id="lastName" name="lastName" placeholder="" class="input-xlarge">
            <p class="help-block">Surname can contain any letters without spaces</p>
        </div>
    </div>
<%--<span>Last Name:</span><br><input type="text" name="lastName" id="lastName"><br>--%>

    <div class="control-group">
        <!-- Button -->
        <div class="controls">
            <button class="btn btn-success" onclick="registration()">Register</button>
            <button class="btn btn-success" onclick="_login();">Use Facebok data</button>
            <button class="btn btn-success" onclick="history.back()"> Cancel </button>
        </div>
    </div>

<%--<input type="button" onclick="registration()" value="Add">
<button onclick="history.back()">Cancel</button>--%>

<script>
    function registration(event) {
        let email = document.getElementById("email").value;
        let password = document.getElementById("password").value;
        let firstName = document.getElementById("firstName").value;
        let lastName = document.getElementById("lastName").value;

        let session = {email,password,firstName,lastName};

        let sessionStr = JSON.stringify( session );

        let options = {

            mode: "cors"
        };

        fetch('http://localhost:8080/movie/list/registration', {
            method : "post",
            headers: {
                "Accept":"application/json",
                "Content-Type" : "application/json"
            },
            body: JSON.stringify(session)
        })
        //        .then(resp => resp.json())
            .then(res => {
                if(res.ok){
                    location.href="http://localhost:8080/movie/list/film-list"
        }else {alert("bad request")}
        }).catch(error => console.error(error));
    }
</script>
</body>

</html>
